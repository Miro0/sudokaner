import React, {PureComponent} from 'react';
import {Alert, View} from 'react-native';
import {
    AdMobBanner,
} from 'react-native-admob'
import BottomButton from './src/BottomButton';
import Sudoku from './src/Sudoku/Sudoku';
import Scan from './src/Scan';
import Processing from './src/Processing';

const APP_STATE_SUDOKU = 0;
const APP_STATE_SCAN = 1;
const APP_STATE_PROCESSING = 2;

export default class App extends PureComponent {
    state = {
        appState: APP_STATE_SUDOKU
    };

    sudoku = React.createRef();
    scan = React.createRef();
    processing = React.createRef();

    componentDidMount() {
        //78 Dla początkujących
        const exampleSudoku = [
            {x: 0, y: 0, value: 6}, //debug
            {x: 1, y: 0, value: 2}, //debug
            {x: 2, y: 0, value: 7},
            {x: 3, y: 0, value: 8},
            {x: 4, y: 0, value: 5}, //debug
            {x: 5, y: 0, value: 9}, //debug
            {x: 6, y: 0, value: 4}, //debug
            {x: 7, y: 0, value: 3},
            {x: 8, y: 0, value: 1}, //debug
            // {x: 0, y: 1, value: 3}, //debug
            {x: 1, y: 1, value: 4},
            // {x: 2, y: 1, value: 9}, //debug
            // {x: 3, y: 1, value: 1}, //debug
            {x: 4, y: 1, value: 6},
            // {x: 5, y: 1, value: 7}, //debug
            {x: 6, y: 1, value: 2},
            // {x: 7, y: 1, value: 5}, //debug
            {x: 8, y: 1, value: 8},
            // {x: 0, y: 2, value: 8}, //debug
            {x: 1, y: 2, value: 5},
            // {x: 2, y: 2, value: 1}, //debug
            // {x: 3, y: 2, value: 4}, //debug
            // {x: 4, y: 2, value: 3}, //debug
            {x: 5, y: 2, value: 2},
            // {x: 6, y: 2, value: 7}, //debug
            // {x: 7, y: 2, value: 6}, //debug
            // {x: 8, y: 2, value: 9}, //debug
            {x: 0, y: 3, value: 1},
            {x: 4, y: 3, value: 2},
            {x: 8, y: 3, value: 6},
            {x: 3, y: 4, value: 3},
            {x: 5, y: 4, value: 4},
            {x: 0, y: 5, value: 5},
            {x: 4, y: 5, value: 7},
            {x: 8, y: 5, value: 4},
            {x: 3, y: 6, value: 7},
            {x: 7, y: 6, value: 8},
            {x: 0, y: 7, value: 2},
            {x: 2, y: 7, value: 6},
            {x: 4, y: 7, value: 4},
            {x: 7, y: 7, value: 9},
            {x: 1, y: 8, value: 8},
            {x: 5, y: 8, value: 3},
            {x: 6, y: 8, value: 6},
        ];

        if (this.sudoku.current.setMatrix) {
            this.sudoku.current.setMatrix(exampleSudoku);

            setTimeout(() => {
                // this.sudoku.current.solveSudoku();
            }, 1000);
        }
    }

    render() {
        const mainAdId = "ca-app-pub-9988273080007404/6809995324";

        return (
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <Sudoku
                        ref={this.sudoku}
                        visible={this.state.appState === APP_STATE_SUDOKU}
                    />
                    <Scan
                        ref={this.scan}
                        visible={this.state.appState === APP_STATE_SCAN}
                    />
                    <Processing
                        ref={this.processing}
                        visible={this.state.appState === APP_STATE_PROCESSING}
                        sudoku={this.sudoku}
                    />
                </View>

                {[APP_STATE_SCAN, APP_STATE_SUDOKU].indexOf(this.state.appState) >= 0 &&
                    <View
                        style={{height: 60, width: '100%', flexDirection: 'row', alignSelf: 'flex-end', backgroundColor: '#222'}}
                    >
                        {this.state.appState === APP_STATE_SUDOKU &&
                        <BottomButton
                            // label={'Rozwiąż'}
                            label={'Start/Stop'}
                            icon={'ios-play'}
                            onPress={() => {
                                this.sudoku.current.toggleRunning();
                            }}
                        />
                        }
                        {this.state.appState === APP_STATE_SCAN &&
                        <BottomButton
                            label={'Skanuj'}
                            icon={'ios-git-network'}
                            onPress={() => {
                                this.scan.current.scan();
                            }}
                        />
                        }
                    </View>
                }

                <View
                    style={{height: 60, width: '100%', flexDirection: 'row', backgroundColor: '#222'}}
                >
                    <BottomButton
                        label={"Sudoku"}
                        icon={"md-grid"}
                        onPress={() => {
                            this.setState({appState: APP_STATE_SUDOKU})
                        }}
                        active={this.state.appState === APP_STATE_SUDOKU}
                    />
                    <BottomButton
                        label={"Skan"}
                        icon={"ios-qr-scanner"}
                        onPress={() => {
                            this.setState({appState: APP_STATE_SCAN})
                        }}
                        active={this.state.appState === APP_STATE_SCAN}
                    />
                </View>
                <AdMobBanner
                    adSize="fullBanner"
                    adUnitID={mainAdId}
                    testDevices={[AdMobBanner.simulatorId]}
                    onAdFailedToLoad={() => {
                        Alert.alert('AdMob - nie działa')
                    }}
                />
            </View>
        );
    };
}
