import React from 'react';
import Grid from './Grid';

export default class Matrix {
    grids: [];

    setMatrix(data) {
        this.grids = [];

        for (let x = 0; x < 9; ++x) {
            for (let y = 0; y < 9; ++y) {
                const grid = data.filter(row => row.x == x && row.y == y);
                if (grid.length === 1) {
                    this.grids.push(new Grid(x, y, grid[0].value, true, false, true));
                } else {
                    this.grids.push(new Grid(x, y, null, false, true, false));
                }
            }
        }
    }

    getGrid(x, y) {
        return this.grids.filter(grid => grid.x == x && grid.y == y)[0];
    }

    getLineX(x) {
        return this.grids.filter(grid => grid.x == x);
    }

    getLineY(y) {
        return this.grids.filter(grid => grid.y == y);
    }

    getSubGrid(subGrid) {
        return this.grids.filter(grid => grid.subGrid == subGrid);
    }

    getAvailableDigits(x, y, biggerThan = 0) {
        let digits = [1, 2, 3, 4, 5, 6, 7, 8, 9];

        //line X
        this.getLineX(x).forEach(grid => {
            digits = digits.filter(digit => digit != grid.value);
        });

        //line Y
        this.getLineY(y).forEach(grid => {
            digits = digits.filter(digit => digit != grid.value);
        });

        //grid 3x3
        const currentGrid = this.getGrid(x, y);
        this.getSubGrid(currentGrid.subGrid).forEach(grid => {
            digits = digits.filter(digit => digit != grid.value);
        });

        if (biggerThan > 0) {
            digits = digits.filter(digit => digit > biggerThan);
        }

        return digits;
    };

    validateField(x, y) {
        const gridToValidate = this.getGrid(x, y);

        //line X
        this.getLineX(x).forEach(grid => {
            if (grid.x !== x && grid.value == gridToValidate.value) {
                return false;
            }
        });

        //line Y
        this.getLineY(y).forEach(grid => {
            if (grid.y !== y && grid.value == gridToValidate.value) {
                return false;
            }
        });

        //grid 3x3
        this.getSubGrid(gridToValidate.subGrid).forEach(grid => {
            if (grid.x !== x && grid.y !== y && grid.value == gridToValidate.value) {
                return false;
            }
        });

        return true;
    }

    insertDynamicValue(x, y, value) {
        // console.log('insertDynamicValue:', x, y, value);

        this.grids = this.grids.map(grid => {
            if (grid.x == x && grid.y == y) {
                grid.setValue(value)
                    .setIsDynamic(true)
                    .setIsVisible(true); //debug
            }

            return grid;
        });
    }

    setNotDynamic(x, y) {
        this.grids = this.grids.map(grid => {
            if (grid.x == x && grid.y == y) {
                grid.setIsDynamic(false);
            }

            return grid;
        });
    }

    setVisibleAll(isVisible = true) {
        this.grids = this.grids.map(grid => {
            grid.setIsVisible(isVisible);
        });
    }
};
