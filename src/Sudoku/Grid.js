import React from 'react';

export default class Grid {
    availableDigits = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    x;
    y;
    value = 0;
    isDynamic: false;
    isStatic: false;
    isVisible: false;
    subGrid: null;

    constructor(x, y, value, isStatic, isDynamic, isVisible) {
        this.x = x;
        this.y = y;
        this.value = value;
        this.isStatic = isStatic;
        this.isDynamic = isDynamic;
        this.isVisible = isVisible;

        this.setSubGrid(x, y);
    }

    setIsDynamic(isDynamic) {
        this.isDynamic = isDynamic;

        return this;
    }

    setIsVisible(isVisible) {
        this.isVisible = isVisible;

        return this;
    }

    setValue(value) {
        this.value = value;

        return this;
    }

    removeAvailableDigit(digits) {
        this.availableDigits = this.availableDigits.filter(availableDigit => digits.indexOf(availableDigit) < 0);

        return this;
    }

    countAvailableDigits() {
        return this.availableDigits.length;
    }

    setSubGrid(x, y) {
        if (x < 3 && y < 3) {
            this.subGrid = 0;
        } else if (x >= 3 && x < 6 && y < 3) {
            this.subGrid = 1;
        } else if (x >= 6 && y < 3) {
            this.subGrid = 2;
        } else if (x < 3 && y >= 3 && y < 6) {
            this.subGrid = 3;
        } else if (x >= 3 && x < 6 && y >= 3 && y < 6) {
            this.subGrid = 4;
        } else if (x >= 6 && y >= 3 && y < 6) {
            this.subGrid = 5;
        } else if (x < 3 && y >= 3 && y >= 6) {
            this.subGrid = 6;
        } else if (x >= 3 && x < 6 && y >= 6) {
            this.subGrid = 7;
        } else if (x >= 6 && y >= 6) {
            this.subGrid = 8;
        }
    }

};
