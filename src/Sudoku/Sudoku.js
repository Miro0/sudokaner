import React, {Component} from 'react';
import {Alert, View, Text, Dimensions, TouchableOpacity} from 'react-native';
import Matrix from './Matrix';
import BottomButton from '../BottomButton';

const STRATEGY_AVAILABLE_DIGITS = 0;
const STRATEGY_INSERT_VALUE = 1;

export default class Sudoku extends Component {
    state = {
        matrix: new Matrix(),
        selectedGrid: null,
        lastDirection: 'next',
        lastX: 0,
        lastY: 0,
        // strategy: STRATEGY_AVAILABLE_DIGITS,
        running: false,
    };

    setMatrix(data) {
        const matrix = new Matrix();
        matrix.setMatrix(data);
        this.setState({matrix});
    }

    getGrid(x, y) {
        return this.state.matrix.getGrid(x, y);
    }

    solveSudoku() {
        this.setState({running: true}, () => {
            // this.solveStep(0, 0);
            this.solveStep(0, 1);
        });

        // console.log('Sudoku should be solved');
    }

    solveStep(x, y) {
        if (this.state.running) {
            this.setState({
                lastX: x,
                lastY: y,
            }, () => {
                setTimeout(() => {
                    const grid = this.getGrid(x, y);

                    console.log(x, y);

                    if (grid !== undefined && (!grid.isDynamic || grid.isStatic)) {
                        if (this.state.lastDirection === 'next') {
                            this.solveStepNext(x, y);
                        } else {
                            this.solveStepBack(x, y);
                        }
                    } else {
                        const nextDynamicValues = this.state.matrix.getAvailableDigits(x, y, grid ? parseInt(grid.value) : 0);

                        console.log('nextDynamicValues', nextDynamicValues);

                        if (nextDynamicValues.length > 0) {
                            this.insertValue(x, y, nextDynamicValues[0])
                                .then(() => {

                                    // this.state.matrix.insertDynamicValue(x, y, nextDynamicValues[0]);

                                    if (nextDynamicValues.length === 1 && this.hasNoDynamicGridsBefore(x, y)) {
                                        // this.matrix.setNotDynamic(x, y);
                                    }

                                    if (this.state.matrix.validateField(x, y)) {
                                        if (this.canDoNextStep(x, y)) {
                                            this.solveStepNext(x, y);
                                        } else {
                                            // console.log('in here?');
                                            return true;
                                        }
                                    } else {
                                        // console.log('invalid');

                                        this.insertValue(x, y, 0)
                                            .then(() => {

                                                // this.matrix.insertDynamicValue(x, y, 0);
                                                this.solveStepBack(x, y);
                                            });
                                    }
                                });
                        } else {
                            // console.log('no next dynamic values');

                            this.insertValue(x, y, 0)
                                .then(() => {

                                    // this.matrix.insertDynamicValue(x, y, 0);
                                    this.solveStepBack(x, y);
                                });
                        }
                    }
                }, 1);
            });
        }
    }

    insertValue(x, y, value) {
        return new Promise((resolve, reject) => {

            this.setState(state => {
                let matrix = state.matrix;
                matrix.insertDynamicValue(x, y, value);
                state.matrix = matrix;
                if (x == 8 && y == 8) {
                    state.running = false;
                }
                return state;
            }, () => {
                // this.refresh();
                resolve();
            });
        });
    }

    solveStepNext(x, y) {
        this.setState({
            lastDirection: 'next',
        }, () => {
            if (x >= 8 && y < 8) {
                y = y + 1;
                x = 0;
            } else {
                x = x + 1;
                // console.log('Added to x:', x);
            }

            if (x > 8) {
                //cant go more forward
            }

            this.solveStep(x, y);
        });
    }

    solveStepBack(x, y) {
        this.setState({
            lastDirection: 'back',
        }, () => {

            if (x <= 0 && y > 0) {
                y = y - 1;
                x = 8;
            } else {
                x = x - 1;
            }

            if (x < 0) {
                //cant go more back
            }

            this.solveStep(x, y);
        });
    }

    canDoNextStep(x, y) {
        return x < 9 && y < 9;
    }

    hasNoDynamicGridsBefore(x, y) {
        return this.state.matrix.grids.filter(grid => {
            return grid.isDynamic === true && (
                grid.y < y || (grid.y == y && grid.x <= x)
            );
        }).length === 0;
    }

    getGridsToRender() {
        const grids = [];
        const screen = Dimensions.get('screen');
        const gridSize = (screen.width - 20) / 9;

        const topOffset = parseInt((screen.height - (gridSize * 9)) / 2) - 65;
        const leftOffset = parseInt((screen.width - (gridSize * 9)) / 2);

        if (this.state.matrix && this.state.matrix.grids) {
            this.state.matrix.grids.forEach(grid => {
                const isSelectedGrid = this.isSelectedGrid(grid.x, grid.y);

                grids.push(
                    <TouchableOpacity
                        key={grid.x.toString() + grid.y.toString()}
                        activeOpacity={grid.isStatic ? 1 : 0.8}
                        onPress={() => {
                            if (!grid.isStatic) {
                                this.setState({
                                    selectedGrid: {x: grid.x, y: grid.y}
                                });
                            } else {
                                this.setState({
                                    selectedGrid: null,
                                });
                            }
                        }}
                        style={{
                            width: gridSize,
                            height: gridSize,
                            borderTopWidth: [0, 3, 6].indexOf(grid.y) >= 0 ? 2 : 1,
                            borderLeftWidth: [0, 3, 6].indexOf(grid.x) >= 0 ? 2 : 1,
                            borderRightWidth: [2, 5, 8].indexOf(grid.x) >= 0 ? 2 : 1,
                            borderBottomWidth: [2, 5, 8].indexOf(grid.y) >= 0 ? 2 : 1,
                            borderColor: '#333',
                            position: 'absolute',
                            top: (grid.y * gridSize) + topOffset - (isSelectedGrid ? 10 : 0),
                            left: (grid.x * gridSize) + leftOffset,
                            backgroundColor: '#eee',
                            elevation: isSelectedGrid ? 3 : 0,
                        }}
                    >
                        <Text style={{
                            color: grid === undefined || grid.isStatic ? '#333' : (grid.isDynamic ? 'green' : 'blue'),
                            textAlign: 'center',
                            marginTop: gridSize / 8,
                            fontSize: gridSize / 2,
                        }}>
                            {grid !== undefined && (!grid.isDynamic || grid.isVisible) ? grid.value : ''}
                        </Text>
                    </TouchableOpacity>,
                );
            });
        }

        return grids;
    }

    toggleRunning() {
        this.setState(state => {
            state.running = !state.running;
            return state;
        }, () => {
            if (this.state.running) {
                if (this.state.lastDirection === 'next') {
                    this.solveStepNext(this.state.lastX, this.state.lastY);
                } else {
                    this.solveStepBack(this.state.lastX, this.state.lastY);
                }
            }
        });
    }

    isSelectedGrid(x, y) {
        if (this.state.selectedGrid === null) {
            return false;
        } else {
            return this.state.selectedGrid.x == x && this.state.selectedGrid.y == y;
        }
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        const grids = this.getGridsToRender();

        return (
            <View
                onLayout={event => {
                    console.log('Layout:', event.nativeEvent.layout);
                }}
                style={{
                    flex: this.props.visible ? 1 : 0,
                    flexDirection: 'row',
                    backgroundColor: '#ddd',
                }}
            >
                {grids}
            </View>
        );
    }
};
