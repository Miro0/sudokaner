import React, {PureComponent} from 'react';
import {View, Text} from 'react-native';

export default class Processing extends PureComponent {
    render() {

        return (
            <View style={{
                flex: this.props.visible ? 1 : 0,
            }}>
                <View style={{flex: 1}}>
                    <Text style={{color: '#333', flex: 1, alignSelf: 'center'}}>Processing</Text>
                </View>
            </View>
        );
    }
};
