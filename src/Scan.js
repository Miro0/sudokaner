import React, {PureComponent} from 'react';
import {Alert, View, StyleSheet, Dimensions, Image} from 'react-native';
import ImageEditor from '@react-native-community/image-editor';
import {RNCamera} from 'react-native-camera';
import OpenCV from './Libs/OpenCV';
import BottomButton from './BottomButton';

export default class Scan extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            sudokuGridWidth: 0,
            sudokuGridHeight: 0,
            sudokuGridUnit: 0,
            sudokuMatrix: [],
            scanMode: true,
            imagesToPreview: [],
        };
    }

    takePicture = async () => {
        if (this.camera) {
            const options = {quality: 0.5, base64: true};
            const data = await this.camera.takePictureAsync(options);

            OpenCV.getSudoku(data.base64, error => {
                Alert.alert('Błąd:' + error);
            }, msg => {
                Alert.alert('getSudoku() nie wyrzuciło żadnego błędu');
            });

            return data;
        }
    };

    scan() {
        this.takePicture().then(imageData => {

            const imageProcessors = [];

            for (let y = 0; y < 9; ++y) {
                for (let x = 0; x < 9; ++x) {
                    imageProcessors.push(new Promise((resolve, reject) => {
                            const {sudokuGridUnit} = this.state;

                            // console.log('crop:', {
                            //     offset: {x: (10 + (x * sudokuGridUnit)), y: (10 + (y * sudokuGridUnit))},
                            //     size: {width: sudokuGridUnit, height: sudokuGridUnit}
                            // });

                            ImageEditor.cropImage(imageData.uri, {
                                offset: {x: (10 + (x * sudokuGridUnit)), y: (10 + (y * sudokuGridUnit))},
                                size: {width: sudokuGridUnit, height: sudokuGridUnit},
                                displaySize: {width: sudokuGridUnit, height: sudokuGridUnit},
                                resizeMode: 'contain',
                            }).then(croppedImage => {
                                this.setState(state => {
                                    const imagesToPreview = [...state.imagesToPreview];
                                    imagesToPreview.push({x, y, croppedImage});
                                    state.imagesToPreview = imagesToPreview;

                                    console.log('new cropped:', imagesToPreview);

                                    return state;
                                }, () => {
                                    resolve();
                                });
                            }).catch(() => {

                                // console.log('ERROR');

                                resolve();
                            });
                        }),
                    );
                }
            }

            Promise.all(imageProcessors).then(() => {
                console.log('Data processed:', this.state.imagesToPreview);

                this.setState({scanMode: false});
            });


            // ImageEditor.cropImage(imageData.uri, {
            //     offset: {x: 10, y: 10},
            //     size: {width: 100, height: 100},
            //     displaySize: {width: 100, height: 100},
            //     resizeMode: 'contain',
            // }).then(croppedImage => {
            //     this.setState({
            //         imageToPreview: croppedImage,
            //         scanMode: false,
            //     });
            // }).catch(error => {
            //     console.log('error while cropping', error);
            // });
        });
    }

    render() {
        const grids = [];
        for (let x = 0; x < 9; ++x) {
            for (let y = 0; y < 9; ++y) {
                if (this.state.scanMode) {
                    grids.push(
                        <View
                            key={x.toString() + y.toString()}
                            style={{
                                position: 'absolute',
                                top: y * this.state.sudokuGridUnit,
                                left: x * this.state.sudokuGridUnit,
                                width: this.state.sudokuGridUnit,
                                height: this.state.sudokuGridUnit,
                                borderWidth: 1,
                                borderColor: '#fff',
                                backgroundColor: 'transparent',
                            }}
                        />,
                    );
                } else {
                    const image = this.state.imagesToPreview.filter(img => img.x == x && img.y == y)[0];
                    if (image && image.croppedImage) {
                        grids.push(
                            <Image
                                key={'image' + x.toString() + y.toString()}
                                style={{
                                    position: 'absolute',
                                    top: y * this.state.sudokuGridUnit,
                                    left: x * this.state.sudokuGridUnit,
                                    width: this.state.sudokuGridUnit,
                                    height: this.state.sudokuGridUnit,
                                    borderWidth: 1,
                                    borderColor: '#fff',
                                    backgroundColor: 'transparent',
                                }}
                                source={{uri: image.croppedImage}}
                            />,
                        );
                    }
                }
            }
        }

        return (
            <View
                style={[styles.container, {
                    flex: this.props.visible ? 1 : 0,
                }]
                }
                onLayout={(event) => {
                    this.setState({
                        sudokuGridWidth: event.nativeEvent.layout.width - 20,
                        sudokuGridHeight: event.nativeEvent.layout.width - 20,
                        sudokuGridUnit: parseInt((event.nativeEvent.layout.width - 20) / 9),
                    });
                }}
            >
                {
                    this.state.scanMode &&
                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        type={RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                        captureAudio={false}
                    >
                        <View style={{
                            position: 'absolute',
                            top: 10,
                            left: 10,
                            height: this.state.sudokuGridHeight,
                            width: this.state.sudokuGridWidth,
                            backgroundColor: 'rgba(0, 0, 0, 0.5)',
                        }}
                        >
                            {grids}
                        </View>
                    </RNCamera>
                }

                {
                    !this.state.scanMode &&
                    <View style={{
                        position: 'absolute',
                        top: 10,
                        left: 10,
                        height: this.state.sudokuGridHeight,
                        width: this.state.sudokuGridWidth,
                    }}
                    >
                        {grids}
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
