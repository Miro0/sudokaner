import React, {PureComponent} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class BottomButton extends PureComponent {
    render() {
        return (
            <TouchableOpacity
                style={{flex: 1, backgroundColor: this.props.active ? '#333' : '#eee'}}
                onPress={this.props.onPress}
            >
                <Icon
                    style={{ textAlign: 'center', marginTop: 10, }}
                    name={this.props.icon}
                    size={24}
                    color={this.props.active ? '#eee' : '#333'}
                />
                <Text style={{
                    color: this.props.active ? '#eee' : '#333',
                    width: '100%',
                    textAlign: 'center',
                    fontSize: 16,
                }}>
                    {this.props.label}
                </Text>
            </TouchableOpacity>
        );
    }
}
